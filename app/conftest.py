# pylint: disable=redefined-outer-name
import pytest

from app import create_app
from model import Group, Car


@pytest.fixture(scope="module")
def test_client():
    flask_app = create_app()
    flask_app.config.from_object("settings.TestingConfig")
    # Flask provides a way to test your application by exposing the Werkzeug test Client
    # and handling the context locals for you.
    testing_client = flask_app.test_client()
    # Establish an application context before running the tests.
    with flask_app.app_context():
        yield testing_client  # this is where the testing happens!


@pytest.fixture
def groups():
    return [
        Group(Id=0, passengers=5),
        Group(Id=1, passengers=3),
        Group(Id=2, passengers=4),
        Group(Id=3, passengers=2),
    ]


@pytest.fixture
def cars():
    return [
        Car(id=0, max_seats=4),
        Car(id=1, max_seats=6),
        Car(id=2, max_seats=5),
        Car(id=3, max_seats=4),
    ]
