from dataclasses import dataclass
from typing import Optional


class Car:
    def __init__(self, id: int, max_seats: int):
        self.id = id
        self.max_seats = max_seats
        self.free_seats = max_seats

    def __repr__(self):
        return f"<Car {self.id} with {self.free_seats} free seats>"

    def __eq__(self, other):
        if not isinstance(other, Car):
            return False
        return other.id == self.id

    def __hash__(self):
        return hash(self.id)

    def __gt__(self, other):
        if self.free_seats == 0:
            return False
        if other.free_seats == 0:
            return True
        if self.free_seats == other.free_seats:
            return self.max_seats > other.max_seats
        return self.free_seats > other.free_seats

    def allocate(self, passengers: int):
        if self.can_allocate(passengers):
            self.free_seats = self.free_seats - passengers

    def deallocate(self, passengers: int):
        self.free_seats = self.free_seats + passengers

    def can_allocate(self, passengers) -> bool:
        return self.free_seats >= passengers


@dataclass
class Group:
    Id: int
    passengers: int
    assigned_to: Optional[Car] = None


class GroupNotFound(Exception):
    pass


class GroupWithoutCar(Exception):
    pass
