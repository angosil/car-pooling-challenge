class Config:
    DEBUG = False
    TESTING = False


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    # Disable forms CSRF protection (just for testing purposes!)
    WTF_CSRF_ENABLED = False
