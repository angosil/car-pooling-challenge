import logging

from flask import Flask, Response, jsonify, request

import model
from log_setup import setup as log_setup
from services import Service, GroupDoesNotExistError

log_setup()


class App:
    def __init__(self):
        self.service = Service()

    @staticmethod
    def status():
        return Response(status=200)

    def cars(self):
        logging.debug('Adding cars')
        if request.content_type != "application/json":
            return Response(status=400)
        try:
            cars = request.get_json()
            cars_to_load = []
            for car in cars:
                cars_to_load.append(model.Car(car["id"], max_seats=car["seats"]))
            self.service.add_cars(cars_to_load)
        except Exception as error:
            return jsonify({"message": str(error)}), 400
        return Response(status=200)

    def journey(self):
        logging.debug("Setup a journey")
        if request.content_type != "application/json":
            return Response(status=400)
        try:
            self.service.journey(
                request.json["id"], request.json["people"]
            )
        except Exception as error:
            return jsonify({"message": str(error)}), 400
        return Response(status=200)

    def drop_off(self):
        logging.debug("Dropping group")
        if request.content_type != "application/x-www-form-urlencoded":
            return Response(status=400)
        try:
            group_id = int(request.form.get("ID"))
            self.service.drop_off(group_id)
        except GroupDoesNotExistError:
            return Response(status=404)
        except Exception as error:
            return jsonify({"message": str(error)}), 400
        self.service.reassign()
        return Response(status=200)

    def locate(self):
        if request.content_type != "application/x-www-form-urlencoded":
            return Response(status=400)
        try:
            group_id = int(request.form.get("ID"))
            logging.debug(f"Locate {group_id=}", )
            group = self.service.get_group(group_id)
            if not group.assigned_to:
                return Response(status=204)
        except GroupDoesNotExistError:
            return Response(status=404)
        except Exception as error:
            return jsonify({"message": str(error)}), 400
        car = group.assigned_to.__dict__
        logging.debug(f"Locate {group_id=}, assigned to {car=}")
        return jsonify(car), 200


def create_app():
    flask_app = Flask(__name__, instance_relative_config=True)
    app = App()

    @flask_app.route("/status", methods=["GET"])
    def status():
        return Response(status=200)

    @flask_app.route("/cars", methods=["PUT"])
    def cars():
        return app.cars()

    @flask_app.route("/journey", methods=["POST"])
    def journey():
        return app.journey()

    @flask_app.route("/dropoff", methods=["POST"])
    def dropoff():
        return app.drop_off()

    @flask_app.route("/locate", methods=["POST"])
    def locate():
        return app.locate()

    return flask_app
