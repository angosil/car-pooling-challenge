import pytest

from services import Service, GroupDoesNotExistError


@pytest.fixture
def service(groups, cars):
    service = Service()
    service.add_cars(cars)
    service.add_groups(groups)
    service.journey(0, 5)
    return service


def test_drop_off(service, cars, groups):
    assert service.get_car(0) == cars[0]
    assert service.get_group(2) == groups[2]
    service.drop_off(2)
    try:
        service.get_group(2)
    except GroupDoesNotExistError as error:
        assert str(error) == "The group 2 does not exist"


def test_drop_off_with_journey(service, cars):
    assert service.get_group(0).assigned_to == cars[2]
    service.drop_off(0)
    try:
        service.get_group(0)
    except GroupDoesNotExistError as error:
        assert str(error) == "The group 0 does not exist"
    assert cars[2].free_seats == 5


def test_drop_off_no_group(service):
    try:
        assert service.drop_off(4)
    except GroupDoesNotExistError as error:
        assert str(error) == "The group 4 does not exist"
