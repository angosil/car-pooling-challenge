import pytest

from model import Group
from services import Service


@pytest.fixture(scope="module")
def service():
    return Service()


@pytest.fixture()
def group_id1():
    return Group(Id=1, passengers=2)


def test_add_group(service, group_id1):
    service.add_group(group_id1)
    assert service.groups == [group_id1]


def test_add_second_group(service, group_id1):
    group = Group(Id=2, passengers=6)
    service.add_group(group)
    assert service.groups == [group_id1, group]
