import pytest

from services import Service


@pytest.fixture
def service(groups, cars):
    service = Service()
    service.add_groups(groups)
    service.add_cars(cars)
    return service


def test_journey(service, cars):
    service.journey(0, 5)
    assert service.get_group(0).assigned_to == cars[2]
