from model import Car
from services import Service


def test_add_cars(cars):
    service = Service()
    service.add_cars(cars)
    for index, car in service.cars.items():
        assert cars[index] == car


def test_add_cars_id_error():
    cars = [
        Car(id=0, max_seats=4),
        Car(id=1, max_seats=6),
        Car(id=0, max_seats=4),
    ]
    service = Service()
    try:
        service.add_cars(cars)
    except ValueError as error:
        assert str(error) == '0 is a repeated car ID'
    else:
        assert False, "Same car saved two times"


def test_add_cars_4_seats_error():
    cars = [
        Car(id=1, max_seats=3),
    ]
    service = Service()
    try:
        service.add_cars(cars)
    except ValueError as error:
        assert str(error) == '3 is not a valid seats number'
    else:
        assert False, "Same car saved two times"


def test_add_cars_6_seats_error():
    cars = [
        Car(id=1, max_seats=7),
    ]
    service = Service()
    try:
        service.add_cars(cars)
    except ValueError as error:
        assert str(error) == '7 is not a valid seats number'
    else:
        assert False, "Same car saved two times"
