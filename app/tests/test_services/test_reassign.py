import pytest

from model import Group
from services import Service


@pytest.fixture
def service(groups, cars):
    service = Service()
    service.add_cars(cars)
    service.add_groups(groups)
    return service


def test_reassign(service, cars):
    service.reassign()
    assert service.get_group(0).assigned_to == cars[2]
    assert service.get_group(1).assigned_to == cars[0]
    assert service.get_group(2).assigned_to == cars[3]
    assert service.get_group(3).assigned_to == cars[1]


def test_reassign_new_group(service, cars):
    service.reassign()
    service.add_group(Group(Id=4, passengers=2))
    assert service.get_group(4).assigned_to is None
    service.add_group(Group(Id=5, passengers=4))
    assert service.get_group(5).assigned_to is None
    service.reassign()
    assert service.get_group(4).assigned_to == cars[1]
    assert service.get_group(5).assigned_to is None


def test_reassign_delete_group(service, cars):
    service.reassign()
    service.add_group(Group(Id=4, passengers=2))
    service.add_group(Group(Id=5, passengers=4))
    service.reassign()
    assert service.get_group(4).assigned_to == cars[1]
    assert service.get_group(5).assigned_to is None
    service.drop_off(4)
    service.reassign()
    assert service.get_group(5).assigned_to == cars[1]
