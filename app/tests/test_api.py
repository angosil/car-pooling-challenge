import json


def test_acceptance(test_client):
    # Accepts valid PUT cars request
    data = [{"id": 0, "seats": 4}, {"id": 1, "seats": 6}, {"id": 2, "seats": 5}, {"id": 3, "seats": 4}]
    resp = test_client.put(
        "/cars", data=json.dumps(data), content_type="application/json"
    )
    assert resp.status_code == 200
    assert resp.get_json() is None
    # Accepts valid POST journey request
    data = {"id": 0, "people": 5}
    resp = test_client.post(
        "/journey", data=json.dumps(data), content_type="application/json"
    )
    assert resp.get_json() is None
    assert resp.status_code == 200
    # Accepts valid POST locate request
    data = {"ID": 0}
    resp = test_client.post(
        "/locate", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 200
    assert resp.get_json() == {'free_seats': 0, 'id': 2, 'max_seats': 5}
    # Accepts valid POST dropoff request
    data = {"ID": 0}
    resp = test_client.post(
        "/dropoff", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 200
    assert resp.get_json() is None
