from model import Group, Car
from services import Service, GroupDoesNotExistError


def test_locate(test_client, monkeypatch):
    car = Car(id=2, max_seats=5)
    car.allocate(5)

    def get_group(*args):
        return Group(Id=0, passengers=5, assigned_to=car)

    monkeypatch.setattr(Service, 'get_group', get_group)

    data = {"ID": 2}
    resp = test_client.post(
        "/locate", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 200
    assert resp.get_json() == {'free_seats': 0, 'id': 2, 'max_seats': 5}


def test_locate_no_group(test_client, monkeypatch):
    def get_group(*args):
        raise GroupDoesNotExistError(2)

    monkeypatch.setattr(Service, 'get_group', get_group)

    data = {"ID": 2}
    resp = test_client.post(
        "/locate", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 404
    assert resp.get_json() is None


def test_locate_no_assigned(test_client, monkeypatch):
    def get_group(*args):
        return Group(Id=0, passengers=5)

    monkeypatch.setattr(Service, 'get_group', get_group)

    data = {"ID": 2}
    resp = test_client.post(
        "/locate", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 204
    assert resp.get_json() is None


def test_locate_bad_request(test_client):
    data = {}
    resp = test_client.post(
        "/locate", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 400
    assert resp.get_json() == {
        'message': "int() argument must be a string, a bytes-like object or a number, not 'NoneType'"
    }
