import json


def test_cars_success(test_client):
    cars = [{"id": 1, "seats": 4}, {"id": 2, "seats": 6}, {"id": 3, "seats": 5}, {"id": 4, "seats": 4}]
    resp = test_client.put(
        "/cars", data=json.dumps(cars), content_type="application/json"
    )
    assert resp.status_code == 200
    assert resp.get_json() is None


def test_cars_content_type(test_client):
    cars = [{"id": 1, "seats": 4}, {"id": 2, "seats": 6}, {"id": 3, "seats": 5}, {"id": 4, "seats": 4}]
    resp = test_client.put(
        "/cars", data=json.dumps(cars), content_type="plain/text"
    )
    assert resp.status_code == 400
    assert resp.get_json() is None


def test_cars_error_service_repeated_car(test_client):
    cars = [{"id": 1, "seats": 4}, {"id": 2, "seats": 6}, {"id": 1, "seats": 4}]
    resp = test_client.put(
        "/cars", data=json.dumps(cars), content_type="application/json"
    )
    assert resp.status_code == 400
    assert resp.get_json().get('message') == '1 is a repeated car ID'
