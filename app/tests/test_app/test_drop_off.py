from services import Service


def test_drop_off(test_client, monkeypatch, groups):
    def get_group(*args):
        return groups[0]

    monkeypatch.setattr(Service, 'get_group', get_group)

    def del_group(*args):
        return True

    monkeypatch.setattr(Service, 'del_group', del_group)

    data = {"ID": 0}
    resp = test_client.post(
        " /dropoff", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 200
    assert resp.get_json() is None


def test_drop_off_group_not_found(test_client):
    data = {"ID": 0}
    resp = test_client.post(
        " /dropoff", data=data, content_type="application/x-www-form-urlencoded"
    )
    assert resp.status_code == 404
    assert resp.get_json() is None


def test_drop_off_group_bad_request(test_client):
    data = {"ID": 0}
    resp = test_client.post(
        " /dropoff", data=data, content_type="plain/text"
    )
    assert resp.status_code == 400
    assert resp.get_json() is None
