import json

from services import Service


def test_journey(test_client):
    data = {'id': 0, 'people': 5}
    resp = test_client.post(
        "/journey", data=json.dumps(data), content_type="application/json"
    )
    assert resp.status_code == 200
    assert resp.get_json() is None


def test_journey_with_cars(test_client, monkeypatch, cars):
    def find_car(*args):
        return cars[2]

    monkeypatch.setattr(Service, 'find_car', find_car)

    data = {'id': 0, 'people': 5}
    resp = test_client.post(
        "/journey", data=json.dumps(data), content_type="application/json"
    )
    assert resp.status_code == 200
    assert resp.get_json() is None


def test_journey_bad_request(test_client):
    data = {}
    resp = test_client.post(
        "/journey", data=json.dumps(data), content_type="application/json"
    )
    assert resp.status_code == 400
    assert resp.get_json() == {'message': "'id'"}
