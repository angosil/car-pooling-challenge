#!/bin/bash
# Objective: build and run a development environment using docker-composer
# Description: Include a set of commands to check the code quality and test the code.

COMPOSE_FILE="docker-compose.test.yml"

remove_docker_images() {
  echo "Removing docker images"
  docker-compose -f $COMPOSE_FILE down --rmi local -v
}

if [[ $1 = "-r"  || $1 = "--restart" ]]; then
  echo
  echo "1) Optional project restart"
  remove_docker_images
fi

echo
echo "2) Starting docker containers"
docker-compose -f $COMPOSE_FILE up --build -d || exit


echo
echo "3) Waiting for app service ..."
while ! curl http://127.0.0.1:8084 -m1 -o/dev/null -s ; do
  echo "Waiting ..."
  sleep 1
done

echo
echo "4) Show logs ..."
docker-compose -f $COMPOSE_FILE logs
