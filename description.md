# Motivation

I am writing this motivation file to provide an overview of the motivations and explanations for the general steps I
have taken in this project.

## Project fork

One of the primary motivations behind this project is to create a fork of the existing project. By creating a fork, it
enables us to have the flexibility to create issues and merge requests (MR) specific to our project. This allows us to
manage the project effectively and make necessary modifications without affecting the original project.

## Setting up the Development Environment

To ensure a streamlined development process and facilitate efficient testing and code review, I have established a
development environment based on Docker Compose and a bash script. This environment plays a crucial role as it enables
me to test and validate the code within an isolated environment. Additionally, it allows for local automation processes
similar to continuous integration (CI) systems.

* How to Use.

To utilize the development environment, follow the steps below:

1. Open your terminal.
2. Navigate to the project directory.
3. Execute the following command:

```bash
./scripts/dev_docker_compose.sh -r
```

The flag `-r` or `--restart` serves the purpose of resetting the environment by deleting the Docker images used in this
particular case.

Every time you run the script, the Flake8 code analysis and tests will also be executed automatically, allowing you to
check the code quality and ensure the project's functionality.

If you have made changes only to specific files or packages and there are no major environment changes, it is
recommended to run the script without the `-r` flag. This avoids recreating the entire container and speeds up the setup
process. To do so, use the following command:

```bash
./scripts/dev_docker_compose.sh
```

Running the script without the -r flag preserves the existing container and its dependencies, providing a faster
development environment startup.

By following these steps and considering the appropriate usage of the script's flags, you can efficiently set up your
development environment and continuously assess the quality and functionality of your code.

### Using the Docker Compose Environment

Now that you have the Docker Compose environment set up, you can continue the development process and interact with your
code within the running container. Follow the steps below to enter the container:

1. Open your terminal.
2. Navigate to the project directory.
3. Execute the following command:

```bash
docker-compose -f docker-compose.dev.yml exec development sh
```

This command will open a shell session inside the running container, allowing you to run commands and interact with your
code.

While inside the container, you can perform various development tasks such as running tests, executing scripts, and
debugging. Any changes made to the code will be reflected within the container, ensuring a streamlined development
experience.

Remember to exit the container by typing exit or pressing Ctrl + D when you're finished working within the container.

Using the Docker Compose environment in this manner provides a consistent and isolated development environment, ensuring
that your code runs smoothly across different systems and dependencies.

### Added Flake8

As part of ensuring high code quality within the project, I have incorporated Flake8. Flake8 is a code analysis tool
that checks for adherence to coding standards and identifies potential code issues. By including Flake8 in our
development environment, we can maintain code quality and improve overall project reliability.

### Added pytest with coverage

In addition to Flake8 for code quality analysis, I have incorporated pytest with coverage to ensure comprehensive
testing of the project. pytest is a powerful testing framework in the Python ecosystem that allows you to write concise
and expressive tests. The coverage package measures the code coverage during the test execution, helping us identify
areas of the code that need more testing.

To use pytest with coverage in the development environment, follow the steps below:

1. Ensure you have the development environment set up by following the previous instructions.
2. Within the running container, execute the following command:

```bash
coverage run -m pytest -vv tests/
```

### Adding unit tests

To ensure the functionality and reliability of our code, it is essential to add unit tests for every part of the
project. Unit tests allow us to verify the behavior of individual components or functions and provide a safety net when
making changes or adding new features.

## Adding logs 

To clarify the flow and debug possible bugs, especially in the integration tests, I have added logging functionality to 
the project. Logging helps in understanding the sequence of events and identifying issues during runtime. By logging 
relevant information at various points in the code, we can effectively trace the execution and troubleshoot any errors 
or unexpected behavior. This improves the overall reliability and maintainability of the project.

## Refactor the code 

Through the process of adding unit tests and logs, I have identified areas in the code that can be improved for better 
readability and maintainability. Refactoring the code involves restructuring or rewriting portions of the codebase to 
enhance its overall design, eliminate duplication, and make it more modular and maintainable. By refactoring the code, 
we can achieve cleaner and more understandable code, reduce technical debt, and improve the ease of future development 
and collaboration.

## Running Test Suite with Docker Compose

The `test_docker_compose.sh` script allows developers to conveniently run the test suite provided by the QA 
(Quality Assurance) team. It leverages Docker Compose to build the necessary image and utilizes similar flags used in 
the `dev_docker_compose.sh` script, ensuring a consistent and streamlined testing environment.

To use the "test_docker_compose.sh" script, follow these steps:

1. Open your terminal.
2. Navigate to the project directory.
3. Execute the following command:

```bash
./scripts/test_docker_compose.sh -r
```

The flag -r or --restart serves the purpose of resetting the environment by deleting the Docker images used for testing 
in this specific case.

Similar to the development environment setup, running the script with the -r flag recreates the entire container, 
ensuring a clean testing environment. This helps to prevent any interference or contamination from previous test runs.

Every time you run the script, it will automatically execute the test suite, allowing you to verify the functionality 
and reliability of your project. This ensures that the code meets the required quality standards and helps identify any 
potential issues or regressions.

## Clearing the Environment with "hard_clean.sh"

The `hard_clean.sh` script provides a convenient way to clear the entire environment using Docker. This script ensures 
a thorough cleanup of all Docker containers, images, and related resources, allowing you to start with a clean slate.

To use the `hard_clean.sh` script, follow these steps:

1. Open your terminal.
2. Navigate to the project directory.
3. Execute the following command:

```bash
./scripts/hard_clean.sh
```

Running this script will trigger a series of Docker commands to remove all containers, images, networks, and volumes in 
all system.

## Refactor the project structure

To further enhance the organization and clarity of the project, I propose refactoring the project structure. This 
involves separating the code or application from the infrastructure and configuration tests. By decoupling these 
components, we can achieve better separation of concerns and improve the maintainability and scalability of the project. 
It allows us to focus on the core application logic independently from the deployment and testing infrastructure, 
making it easier to manage and understand the project as it grows.
